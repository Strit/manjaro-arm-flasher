# Manjaro ARM Flasher

This repo contains the source for the Manjaro ARM Flasher tool, written in PyQt5/PyQt6 (Python 3 and QT 5/6).


## Dependencies (Arch package names)
So far only:
* python-pyqt5 (5.12 or newer) or python-pyqt6
* util-linux
* xz
* polkit
* python-requests

## Installation

This repository contains a `install.sh` file, which you can run and it will place the files it needs it the correct places.

### Uninstallation

To remove/uninstall the application if you used the `install.sh` installationscript, you can run `./install.sh uninstall` and it will remove the files from the system that the installation added.

## Usage
```
manjaro-arm-flasher
```

## Disclaimer
This application needs root permissions to flash the image to the block device. So Polkit will present a password box when the application is about to flash the device.

This project also contains a .desktop file, which requires the `manjaro-arm-flasher` script to be placed in `/usr/local/bin/` and marked as executable.

## Known issues
Check the [Issues section](https://gitlab.com/Strit/manjaro-arm-flasher/-/issues) of this gitlab repo.
